# apoBank ChatGPT Demo

## Important things to know

* this is not a finished product - no connection to other inter-bank systems exists
* this is not a program with guaranteed GPDR compliance
    * do **not** use Mails with actual customer information here
    * example mails are provided, alternatively actual customer mails may only be used if stripped manually of personal details beforehand. Since no connections to outside systems exists, names etc. are only pseudomized if they fit the example

## How to run

### 1. Install Python

This project was tested with [Python](https://www.python.org/downloads/) version `3.10.6`, but other versions, especially newer ones might also work. Python `2.x` will definitely *not* work, as will some older versions of Python 3.

### 2. Install required packages

There is a `requirements.txt` file included. Again, other versions of those packages may work if already installed, but to be safe you can install the versions used during testing with the following command: 

```console
$ pip install -r RELATIVE_PATH/requirements.txt
```

### 3. Get an OpenAI API key

In order to provide the key functionality, an OpenAI API Key is required in order to make requests to ChatGPT. This API Key is usually stored in

```
<PATH_TO_PROJECT>/apobank-chatgpt-demo/data/api.key
```

You will have to create this file(path) yourself and paste the API Key there (pay attention that there are no line breaks at the end!). Such a key can be obtained from the [OpenAI account page](https://platform.openai.com/account/api-keys) after having created an account. Every account includes a free $18 budget for making requests, providing payment information is, at time of writing, unnecessary. This budget should suffice for any demonstration, but since i have been using my private account the key is not included in this GitLab project.


### 4. Run the project

Navigate to the path of the project (`apobank-chatgpt-demo/`) and execute

```console
$ python3 -m app.run
```

or

```console
$ python -m app.run
```

depending on your python installation. Within the terminal, there will be an URL displayed. Enter this URL in your browser (or press CTRL + Left Click). The home page you will see is a stub, use the navigation bar on top to find the actual application.