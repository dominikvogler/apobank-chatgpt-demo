from flask import Flask, render_template, jsonify, request
from app.chat.chat_gpt_interface import ChatGPTInterface
from app.chat.text_replacement import TextReplacement
from app.chat.handlers.sentiment_analysis_handler import SentimentAnalysisHandler
from os import path

_API_KEY_LOCATION_ = path.join('data', 'api.key')
_ROLE_ADVISOR_ = 'Sei und verhalte dich wie ein Bankberater, welcher Mails zur Beantwortung der Schreiben von Kunden verfasst. Die Bank, bei der du arbeitest, heißt "apoBank". Dein Name ist "Berater_01", deine Mail-Adresse lautet "Berater_01@apobank.de", deine Telefonnummer ist "0211 1234567".'
_ROLE_ADVISOR_MAIL_ = 'Du bist ein Bankberater. Stufe für die folgende Mail auf einer Skala von 1 bis 10 ein wie zufrieden oder unzufrieden ein Kunde ist. 1 steht für einen sehr wütenden oder frustrierten Kunden, während 10 ein glücklicher Kunde ist. Gib nur die Zahl und nichts anderes aus. Die Ausgabe soll also zum Beispiel für einen ziemlich zufriedenen Kunden nur "9" sein.'

app = Flask(__name__)

chatgpt = ChatGPTInterface(ChatGPTInterface.get_api_key(_API_KEY_LOCATION_), _ROLE_ADVISOR_)
chat_sentimentAnalysis = ChatGPTInterface(ChatGPTInterface.get_api_key(_API_KEY_LOCATION_), _ROLE_ADVISOR_MAIL_)

# here, keywords can be given whcih should be pseudomized within the text. in an actual use case, these would come from another systempip
text_replacement = TextReplacement(['apoBank', 'Meiner', 'Berater_01@apobank.de', 'Berater_01', '0211 1234567'])
_ROLE_ADVISOR_ = text_replacement.pseudomize(_ROLE_ADVISOR_)

with open("role.log", "a+", encoding='utf-8') as logfile:
    logfile.write(_ROLE_ADVISOR_ + "\n")

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/example/kundenmail')
def example_kundenmail():
    return render_template('example-kundenmail.html')
"""
@app.route('/profile/<name>')
def profile(name):
    if name == None:
        name == ""
    return render_template('profile.html', name=name)

@app.route('/profile')
def profile_empty():
    return render_template('profile.html', name='') 
"""
@app.route('/chat/handle_customerMailForm', methods=['POST'])
def handle_customerMailForm():
    if request.method == 'POST':
        chatGPTQuery = text_replacement.pseudomize(request.form.get("chatGPTPrompt"))
        chatGPTResponse = chatgpt.get_prompt(chatGPTQuery)
        modifiedChatGPTResponse = text_replacement.de_pseudomize(chatGPTResponse)
        return jsonify(status="success", modifiedChatGPTResponse=modifiedChatGPTResponse, chatGPTQuery=chatGPTQuery, chatGPTResponse=chatGPTResponse)
@app.route('/chat/sentiment_analysis', methods=['POST'])
def handle_sentimentAnalysis():
    if request.method == 'POST':
        chatGPTQuery = text_replacement.de_pseudomize(request.form.get("sentimentAnalysisPrompt"))
        chatGPTResponse = chat_sentimentAnalysis.get_prompt(chatGPTQuery)
        modifiedChatGPTResponse = text_replacement.de_pseudomize(chatGPTResponse)

        
        return jsonify(status="success", modifiedChatGPTResponse=SentimentAnalysisHandler.handle(modifiedChatGPTResponse))
    
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True)