function changeProgressBarColor(progressbar_selector, value) {
    /**
     * pogressbar_id has to have '#' als first character
     */
    value = ((value < 0) ? 0 : value);
    value = ((value > 100) ? 100 : value);

    $(progressbar_selector).removeClass("bg-success bg-info bg-warning bg-danger");
    console.log(value);
    if (value <= 30) {
        $(progressbar_selector).addClass("bg-danger");
    } else if (value <= 60) {
        $(progressbar_selector).addClass("bg-warning");
    } else if (value <= 80) {
        $(progressbar_selector).addClass("bg-info");
    } else if (value <= 100) {
        $(progressbar_selector).addClass("bg-success");
    }
}

function giveCustomerMailExample(example) {
    $('.progress-sentiment').css('width', 0+'%').attr('aria-valuenow', 0);
    $('textarea#customerMailSuggestedAnswer').val('')
    $('textarea#customerMailTextArea').val(example)
}

function getMailtoUrl(to, subject, body) {
    var args = [];
    if (typeof subject !== 'undefined') {
        args.push('subject=' + encodeURIComponent(subject));
    }
    if (typeof body !== 'undefined') {
        args.push('body=' + encodeURIComponent(body))
    }

    var url = 'mailto:' + encodeURIComponent(to);
    if (args.length > 0) {
        url += '?' + args.join('&');
    }
    return url;
}

function sendResponseMail() {
    address = getMailtoUrl('dominikvogler@gmx.de', 'AW: Ihr Anliegen', $('textarea#customerMailSuggestedAnswer').val())
    window.location.href = 'mailto:' + address;
}


window.addEventListener('load', function() {
    $("#chatGPTWaitingAlert").hide();
    var chatGPTResponse = ''
    $('#kundenmail-button-send').click(function(){
        $("#chatGPTWaitingAlert").show();
        $.ajax({
            type: 'POST',
            url: document.getElementById("customerMailForm").action,
            data: {chatGPTPrompt: $('textarea#customerMailTextArea').val()},
            dataType: "text",
            success: function(data){
                        data = $.parseJSON(data);
                        $("#chatGPTWaitingAlert").hide();
                        $("textarea#customerMailSuggestedAnswer").val(data.modifiedChatGPTResponse);

                        $('#chatGPTQuery').text(data.chatGPTQuery)
                        // $('#chatGPTResponse').text(data.originalChatGPTResponse)
                        chatGPTResponse = data.modifiedChatGPTResponse
                     }
          });
    });

    $('#customer-mail-analysis').click(function() {
        $("#chatGPTWaitingAlert").show();
        $.ajax({
            type: 'POST',
            url: document.getElementById("customerMailAnalysisForm").action,
            data: {sentimentAnalysisPrompt: $('textarea#customerMailTextArea').val()},
            dataType: "text",
            success: function(data){
                        data = $.parseJSON(data);
                        $("#chatGPTWaitingAlert").hide();
                        progress = data.modifiedChatGPTResponse * 10;
                        changeProgressBarColor('.progress-sentiment', progress);
                        $('.progress-sentiment').css('width', progress+'%').attr('aria-valuenow', progress);
                     }
          });
    });

    $('#gen-mail-reset').click(function() {
        $('textarea#customerMailSuggestedAnswer').val(chatGPTResponse)
    });

    $("#gen-mail-copy").click(function() {
        var copyText = document.getElementById("customerMailSuggestedAnswer");
        // Select the text field
        copyText.select();
        copyText.setSelectionRange(0, 99999);

        // Copy the text inside the text field
        navigator.clipboard.writeText(copyText.value);
    });
    $("#customer-mail-delete").click(function() {
        $('textarea#customerMailTextArea').val('')
        $('.progress-sentiment').css('width', 0+'%').attr('aria-valuenow', 0);
        $("textarea#customerMailSuggestedAnswer").val('');
    });

    $("#gen-mail-copy").click(function() {
        var copyText = document.getElementById("customerMailSuggestedAnswer");
        // Select the text field
        copyText.select();
        copyText.setSelectionRange(0, 99999);

        // Copy the text inside the text field
        navigator.clipboard.writeText(copyText.value);
    });
    $("#gen-mail-edit-toggle").click(function() {
        if ( $('textarea#customerMailSuggestedAnswer').is('[readonly]') ) { 
            $('textarea#customerMailSuggestedAnswer').attr("readonly", false);
            $('#gen-mail-edit-toggle').html("<i class='fa fa-lock'></i> Sperren");
        }
        else {
            $('textarea#customerMailSuggestedAnswer').attr("readonly", true);
            $('#gen-mail-edit-toggle').html("<i class='fa fa-edit'></i> Bearbeiten");
        }
    });

    $('#btn-sidebar-example-1').click(function() {
        let example = 'Sehr geehrte Damen und Herren,\n\n'
        + 'seit zwei Wochen funktioniert mein Onlinebanking-Zugang nicht. \n'
        + 'Ich kann mich nicht einloggen und daher auch insbesondere meine Überweisungen nicht einsehen, auch wenn dies sehr wichtig für mich ist. Telefonisch kann ich auch niemanden erreichen, da ich stets nur in der Warteschlange hänge. Mein Berater scheint mich gar komplett zu ignorieren! So kann das nicht weitergehen!\n'
        + 'Bitte geben Sie mir schnellstmöglichst Hilfestellung zur Lösung dieses Problems.\n\n'
        + 'Mit freundlichen Grüßen\n'
        + 'Herr Meiner';

        giveCustomerMailExample(example);
    });
    $('#btn-sidebar-example-2').click(function() {
        let example = 'Sehr geehrte Damen und Herren,\n\n'
        + 'ich möchte als Neukunde bei der apoBank ein Girokonto eröffnen. Insbesondere benötige ich einen Online-Banking-Zugang. \n'
        + 'Bitte geben Sie mir Bescheid über die nötigen Schritte und lassen Sie ggf. eine Telefonnummer oder E-Mail-Adresse dar, bei der ich mich bei Bedarf melden kann.\n\n'
        + 'Mit freundlichen Grüßen\n'
        + 'Herr Meiner';

        giveCustomerMailExample(example);
    });
    $('#btn-sidebar-example-3').click(function() {
        let example = 'Sehr geehrter Berater_01 ,\n\n'
        + 'die Depoteröffnung hat funktioniert, ich kann es nun in meinen Online-Banking-Zugang sehen. \n'
        + 'Vielen Dank für die schnelle Eröffnung und Bearbeitung meiner Anträge Ihrerseits.\n\n'
        + 'Mit freundlichen Grüßen\n'
        + 'Herr Meiner';

        giveCustomerMailExample(example);
    });
    
    $("#btn-send-mail").click(function() {
        sendResponseMail();
    });
});
