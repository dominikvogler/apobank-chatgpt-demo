import hashlib
import re
import random

class TextReplacement():
    def __init__(self, sensitive_strings: list = []) -> None:
        self.pseudonimization = dict()              # actual_string -> pseudonym
        self.de_pseudonimization = dict()           # pseudonym -> actual_string

        for s in sensitive_strings:
            self.add_replacement_string(s)

    def pseudomize(self, prompt: str) -> str:
        for actual_string in self.pseudonimization.keys():
            prompt = prompt.replace(actual_string, self.pseudonimization[actual_string])
        return prompt
    
    def replace_pattern(self, prompt: str, pattern: str) -> str:
        p = re.compile(pattern)
        while (matchlist := p.findall(prompt)) is not []:
            for m in matchlist:
                if m not in self.pseudonimization.keys():
                    pseudonym = self.add_replacement_string(m)

                    # prevents two different elements having the same key
                    while pseudonym in self.pseudonimization.keys():
                        pseudonym = self.add_replacement_string(m)
                else:
                    pseudonym = self.pseudonimization[m]
                prompt = prompt.replace(prompt, pseudonym)
        return prompt
    
    def pseudomize_email(self, prompt):
        # pattern = r"^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
        pattern = r"^\S+@\S+\.\S+$"

        return self.replace_pattern(prompt, pattern)
    
    def de_pseudomize(self, prompt: str) -> str:
        for pseudonym in self.de_pseudonimization.keys():
            prompt = prompt.replace(pseudonym, self.de_pseudonimization[pseudonym])
        return prompt
    
    def add_replacement_string(self, s: str) -> str:
        if s in self.pseudonimization.keys():
            return self.pseudonimization[s]
        
        pseudonym = self.createPseudonymToken(s)
        # prevents two different elemtns having the same key
        while pseudonym in self.pseudonimization.keys():
                        pseudonym = self.add_replacement_string(s)

        self.pseudonimization[s] = pseudonym
        self.de_pseudonimization[pseudonym] = s
        return pseudonym

    def createPseudonymToken(sel, s: str) -> str:
        hash = str(hashlib.sha256(str(random.getrandbits(128)).encode('utf-8')).hexdigest())
        return f'[{hash[:16]}]'
