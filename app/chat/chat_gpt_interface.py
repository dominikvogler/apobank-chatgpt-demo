import openai
import os

class ChatGPTInterface():
    def __init__(self, api_key: str, role: str) -> None:
        openai.api_key = api_key
        self.dialog = [{'role':'system', 'content':role}]

    def get_prompt(self, question):
        self.dialog.append({'role':'user', 'content':question})
        result = openai.ChatCompletion.create(
            model='gpt-3.5-turbo',
            messages=self.dialog
        )   
        answer = result.choices[0].message.content
        self.dialog.append({'role':'assistant', 'content':answer})
        return answer
    
    @staticmethod
    def get_api_key(path) -> str:
        os.path.dirname(os.path.abspath(__file__))
        with open(path, 'r') as api_key_file:
            API_KEY = api_key_file.read()
        return API_KEY
    
    