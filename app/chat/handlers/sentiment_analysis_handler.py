import re

from app.chat.handlers.handler import Handler

class SentimentAnalysisHandler(Handler):
    @staticmethod
    def handle(answer: str):
        pattern = '[0-9]+'

        if (match := re.search(pattern, answer)):
            number = match.group(0)
        return int(number)
    
