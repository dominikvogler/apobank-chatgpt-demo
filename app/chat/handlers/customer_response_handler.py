from chat.handlers.handler import Handler


class CustomerResponseHandler(Handler):
    @staticmethod
    def prepare(prompt: str) -> str:
        return super().prepare(prompt)
    
    @staticmethod
    def handle(answer: str):
        return super().handle(answer)